<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Utility Point Test</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
                margin-left: 10vw;
                margin-right: 10vw;
            }

            .title {
                font-size: 84px;
            }

            .subtitle {
                font-size: 42px;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            code {
                padding: 8px;
                background: lightgrey;
                border: 1px solid #636b6f;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Utility Point Test
                </div>
                <div class="subtitle m-b-md">
                    Lee Martin
                </div>

                <div class="information">
                    <p>The data import has been set up as a Console Command which is run via the command line:</p>
                    <code>php artisan import:paymentToSuppliers</code>
                    <p>A Cron schedule for the import can be configured within the app\Console\Kernel.php file.<br>
                        I have set up a commented example for this in that file already.</p>
                </div>
            </div>
        </div>
    </body>
</html>
