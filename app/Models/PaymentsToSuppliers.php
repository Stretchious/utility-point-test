<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PaymentsToSuppliers
 * @package App\Models
 * @property $id int unique identifier
 * @property $body_name string
 * @property $organisation_unit string
 * @property $expense_category string
 * @property $expenditure_code int
 * @property $transaction_date date
 * @property $transaction_number string
 * @property $transaction_amount decimal
 * @property $supplier_name string
 */
class PaymentsToSuppliers extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'body_name', 'organisation_unit', 'expense_category',
        'expenditure_code', 'transaction_date', 'transaction_number',
        'transaction_amount', 'supplier_name'
    ];
}
