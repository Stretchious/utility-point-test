<?php

namespace App\Console\Commands;

use App\Models\PaymentsToSuppliers;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

/**
 * Class Import Payment To Suppliers data
 *
 * @package App\Console\Commands
 */
class ImportPaymentsToSuppliers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:paymentsToSuppliers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Payment To Suppliers data from gov.uk';

    /**
     * Url to obtain data from
     *
     * @var string
     */
    protected $url = 'https://ckan.publishing.service.gov.uk/api/action/package_show?id=all-payments-to-suppliers';

    /**
     * Select financial year to download
     *
     * @var string
     */
    protected $year = '2011/2012';

    /**
     * Http raw response object
     *
     * @var Http
     */
    protected $response;

    /**
     * JSON decoded response object
     *
     * @var string
     */
    protected $json;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->response = Http::get($this->url);

        if (!$this->response->ok()) {
            Log::debug('Import Payment to Suppliers Failed: '
                .'The request url returned a status code of '
                . $this->response->status());
        }

        $this->json = $this->response->json();

        if (isset($this->json['result'])
            && !$this->json['result']['num_resources'] === 0
            && !count($this->json['result']['resources']) > 0) {
            Log::debug('Import Payment to Suppliers Failed: '
                . 'No results found in data.');
        }

        foreach ($this->json['result']['resources'] as $resource) {
            $this->processResource($resource);
        }
    }

    /**
     * Process a resource
     *
     * @param array $resource
     * @throw exception Exception
     * @return void
     */
    protected function processResource(array $resource)
    {
        if (str_contains($resource['name'], $this->year)) {
            try {
                if (strtolower($resource['format']) !== 'csv') {
                    throw new \ErrorException('data expected in CSV format but returned in '
                         . $resource['format']);
                }

                $rowCount = 0;
                if ($csvFile = fopen($resource['url'], "r")) {
                    $header = true;

                    while (!feof($csvFile)) {
                        $csvRowData = fgetcsv($csvFile, 1000, ",");
                        if (empty($csvRowData)) {
                            continue;
                        }

                        if ($header) {
                            $header = false;
                        } else {
                            PaymentsToSuppliers::updateOrCreate(
                                [
                                    'id' => implode("", array_reverse(explode("/", $csvRowData[4])))
                                        . $rowCount
                                ],
                                [
                                    'body_name' => $csvRowData[0],
                                    'organisation_unit' => $csvRowData[1],
                                    'expense_category' => $csvRowData[2],
                                    'expenditure_code' => $csvRowData[3],
                                    'transaction_date' => implode("/", array_reverse(explode("/", $csvRowData[4]))),
                                    'transaction_number' => $csvRowData[5],
                                    'transaction_amount' => $csvRowData[6],
                                    'supplier_name' => $csvRowData[7]
                                ]
                            );
                        }

                        $rowCount++;
                    }

                    fclose($csvFile);
                }
            } catch(\Exception $exception) {
                Log::debug('Import Payment to Suppliers Data Entry Failed: '
                    . $exception->getMessage());
            }
        }
    }
}
