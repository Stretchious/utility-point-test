# README #

This repository contains a basic Laravel application for the UtilityPoint 2nds stage interview development test. 

### Requirements ###

* PHP ^7.2.5|^8.0
* MySQL
* Composer

### Setting up ###

* Clone the repository.
* From a command line within the project root directory, run ``composer install`` to install the required dependencies.
* Make a copy of the ``.env.example`` file and rename it to ``.env`` 
* Create an empty MySQL database, including a username and password, then edit the ``.env`` file and add the new MySQL 
database credentials into the relevant sections.
* From the command line, run ``php artisan migrate`` to import the database tables.
* The application can be run from any environment that utilises PHP, using artisan via the command line:
``php artisan serve`` with an optional port number, for example ``php artisan server --port=8100``.
* If desired, the front facing site can be accessed via the loopback ip address and port number 
``http://127.0.0.1:8100`` although this is not specifically required for this project.

### Running ###

The requirements for this task are run via the command line within the project root directory:
```
php artisan import:paymentsToSuppliers
```
A Cron schedule for the import can be also be configured within the ``app\Console\Kernel.php`` file.

A commented example for this has been setup already within that file.

### Logging ###

Any errors encountered are outputted to the standard Laravel Log which is found in the ``storage/logs/laravel.log`` file.

Continuous errors can be viewed within this log file by using either: 
```
tail -f storage/logs/laravel.log
```
or 
```
less +F storage/logs/laravel.log
```

### Developed by ###
Lee Martin
