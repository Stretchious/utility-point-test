<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsToSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments_to_suppliers', function (Blueprint $table) {
            $table->id();
            $table->string('body_name');
            $table->string('organisation_unit');
            $table->string('expense_category');
            $table->unsignedInteger('expenditure_code');
            $table->date('transaction_date');
            $table->string('transaction_number');
            $table->decimal('transaction_amount');
            $table->string('supplier_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments_to_suppliers');
    }
}
